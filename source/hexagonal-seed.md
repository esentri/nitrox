---
size: 1080p
#canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: theme/custom.css
subtitles: embed
voice: Ilse
---
![contain](slides/hexagonal-seed/Folie00.png)

Hallo und herzlich willkommen zum Naitro-X Hexagonal-Seed.

(pause: 1)
---

weitere Infos findet Ihr irgendwo
Vielen Dank für eure Zeit und bis bald!
