---
size: 1080p
#canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: theme/custom.css
subtitles: embed
voice: Ilse
---
![contain](slides/archunit-introduction/Folie00.png)

Hallo und herzlich willkommen zur Einführung von Arc-unit.

(pause: 1)
---
![contain](slides/archunit-introduction/Folie01.png)

Viele Entwickler werden die Geschichte kennen, dass es einmal einen erfahrenen Entwickler gab, der ein paar schöne Architekturdiagramme erstellte, die zeigten aus welchen Komponenten ein System bestehen sollte und wie sie zusammenwirken sollten.

(pause: 1)
---
(video:
  file: slides/archunit-introduction/animation01.mov
  freeze: )

 Aber als das Projekt größer wurde, die Anwendungsfälle komplexer, neue Entwickler hinzukamen und alte Entwickler ausstiegen, gab es immer mehr Fälle, in denen neue Funktionen einfach so hinzugefügt wurden, wie es gerade passte. Und plötzlich hing alles von allem ab und jede Änderung konnte unvorhersehbare Auswirkungen auf jede andere Komponente haben.

 Natürlich könnte man erfahrene Entwickler einsetzen, die sich den Code einmal pro Woche ansehen, Verstöße feststellen und diese korrigieren. Sicherer ist es jedoch, Regeln für diese Komponenten einfach im Code zu definieren, die automatisch getestet werden können.

 (pause: 0.5)

 Hier kommt Arc-unit ins Spiel.

(pause: 1)
---
![contain](slides/archunit-introduction/Folie02.png)

Arc-unit ist eine kostenlose Bibliothek zur Überprüfung der Architektur von tschawa-code. Mit Arc-unit verfügt man über automatische Architekturtests, kann Regeln weiterentwickeln, sehen wo alte Komponenten geändert werden müssen und sicherstellen, dass neue Komponenten dem gemeinsamen Verständnis der Entwickler und Architekten entsprechen. Insgesamt trägt dies zur Qualität der Codebasis bei und verhindert einen Rückgang der Entwicklungsgeschwindigkeit. Außerdem wird es neuen Entwicklern viel leichter fallen, sich mit dem Code vertraut zu machen.

(pause: 1)
---
(font-size: 22)
````md
# EINLEITUNGSBEISPIEL
```java
// JUnit5
@AnalyzeClasses(packages = "nitrox.seed")
public class MyArchUnitTest {

    // JUnit5
    @ArchTest
    static final ArchRule exampleRule = classes()
            .that().resideInAPackage("..service..")
            .should().onlyBeAccessed().byAnyPackage("..controller..", "..service..");

    // JUnit5
    @ArchTest
    public void alternativeRule(TschawaClasses importedClasses) {
        ArchRule exampleRule = classes()
                .that().resideInAPackage("..service..")
                .should().onlyBeAccessed().byAnyPackage("..controller..", "..service..");
        exampleRule.check(importedClasses);
    }

    // Manueller Import
    private final TschawaClasses manualImportedClasses = new ClassFileImporter().importPackages("nitrox.seed");

    @Test
    public void ruleWithManualImportedClasses() {
        ArchRule exampleRule = classes()
                .that().resideInAPackage("..service..")
                .should().onlyBeAccessed().byAnyPackage("..controller..", "..service..");
        exampleRule.check(manualImportedClasses);
    }    
}
```
````

Bevor wir auf einen Überblick der Funktionalitäten von Arc-unit eingehen, wollen wir dies an einem Einleitungsbeispiel zeigen. Eine Arc-unit Testklasse besteht dabei aus 2 Bausteinen. 

Der erste besteht darin die Klassen zu importieren, deren Struktur überprüft werden soll. Dazu analysiert Arc-Unit den gegebenen Tschawa-Bytecode, indem es alle Klassen in eine Tschawa-Code-Struktur importiert. Auf dieser Basis können dann mit Verwendung eines beliebigen Tschawa-Unit-Test-Frameworks Regeln definiert werden und Verstöße aufgezeichnet. In dem zu sehenden Code-Snippet werden Klassen mit Hilfe von Tschey-Unit über die Annotation AnalyzeClasses importiert. Alternativ kann man über einen ClassFileImporter die Klassen importieren.

(pause: 1)  

In der Klasse selbst wird die gleiche Regel auf verschiedene Arten implementiert. Einmal direkt als Feld und die anderen male als Funktion. Alle Regeln prüfen dabei, ob Klassen die in einem Package Service liegen nur von Klassen aus den Packages Controller und Service zugegriffen werden können. Die zwei aufeinanderfolgenden Punkte repräsentieren dabei eine beliebige Anzahl von Packages.


(pause: 1)
---
![contain](slides/archunit-introduction/Folie03.png)

Arc-unit ist intern in verschiedene Schichten unterteilt, von denen die wichtigsten die "Core"-Schicht, die "Lang"-Schicht und die "Library"-Schicht sind. Kurz gesagt befasst sich die Core-Schicht mit der grundlegenden Infrastruktur, d.h. wie man Byte-Code in Tschawa-Objekte importiert. Ein Großteil der Kern-API von Arc-unit ähnelt dabei der Tschawa-Reflection-API. Auf diese Tschawa-Objekte können widerum Methoden aufgerufen werden. Zum Beispiel ist es möglich, programmatisch auf die importierten Zugriffe der Tschawa-Klasse zu iterien und zu reagieren. Allerdings sind Tests, die auf diese Weise geschrieben werden, nicht besonders prägnant. Sie sollten nur genutzt werden, wenn keine andere Funktionalität von Arc-unit zur Verfügung gestellt wird und wird deshalb im weiteren Verlauf der Einführung nicht konkretisiert. Abhilfe schafft die "Lang"-Schicht, die Regelsyntax enthält, um gängige Architekturregeln in knapper Form zu spezifizieren. Die Bibliotheksschicht schließt zusätzlich komplexere vordefinierte Regeln mit ein, wie zum Beispiel eine mehrschichtige Architektur.

(pause: 1)
---
![contain](slides/archunit-introduction/Folie04.png)

Wie im Einführungsbeispiel gesehen, können Regeln sehr kurz definiert werden. Generell lässt sich die Struktur einer solchen Regel-definiton in 3 Bestandteile zerlegen. Den ersten Teil nennen wir Subjekt und besteht darin, welche Objekte überprüft werden soll. Den zweiten Teil nennen wir Prädikat und beschränkt die Objekte auf eine relevante Teilmenge. Der letzte Teil besteht aus einer Bedingung, die die definierten Objekte erfüllen muss. Eine Regel muss dabei nicht zwangsläufig ein Prädikat haben.

(pause: 1)
---
(font-size: 17)
````md
# LANG API: Class Rules
```java
    // Abhängigkeiten zwischen Packages
    @ArchTest
    static final ArchRule packageDependecy = classes()
            .that().resideInAPackage("..service..")
            .should().onlyBeAccessed().byAnyPackage("..controller..", "..service..");

    // Klassen und Package Beinhaltung
    @ArchTest
    static final ArchRule classContainmentRule = classes()
            .that().haveSimpleNameEndingWith("Config")
            .should().resideInAPackage("..config..")

    
    // Klassen Abhängigkeiten
    @ArchTest
    static final ArchRule classDependencyRule = noClasses()
        .that().haveNameMatching(".*Repository")
        .should().dependOnClassesThat().haveSimpleName(".*Controller")

    // Vererbung
    @ArchTest
    static final ArchRule classInterfaceRule = classes()
        .that().implement(BestellungController.class)
        .should().haveSimpleNameEndingWith("Controller");

    @ArchTest
    static final ArchRule classInheritanceRule =  classes()
        .that().areAssignableTo(JooqAggregateRepository.class)
        .should().haveOnlyFinalFields();

    // Annotation
    @ArchTest
    static final ArchRule annotationRule = classes()
            .that().areAnnotatedWith("@Controller")
            .should().resideInAPackage("..controller..");     
```
````

Die wohl am häufigsten zu überprüfenden Subjekte sind Klassen. In dem zu sehenden Code-Snippet sind dabei 5 gängige Tests beispielhaft unterschieden. Wie im Eingangsbeispiel dargestellt kann man dabei Abhängigkeiten zwischen Packages überprüfen.

(pause: 0.5)

Darüber hinaus ist es auch möglich zu kontrollieren, ob per Naming definierte Klassen in einem Package liegen. Damit kann man beispielsweise sicherstellen, dass Konfigurationsklassen nur in spezifischen Konfigurationspaketen liegen.
 
(pause: 0.5)

Außerdem können Abhängigkeiten zwischen Klassen gecheckt werden, wenn man zum Bleistift sicherstellen will, dass Repositoryklassen nicht direkt von Controllerklassen abhängig sein sollen. Dafür wurde in diesem Kontext die Funktion noClasses() genommen, die einfach eine Negation der classes Funktion darstellt.
  
(pause: 0.5)
   
Des Weiteren ist es möglich verschiedene Prüfungen im Zusammenhang mit Vererbung zu definieren. Es können z.B. Namenskonventionen definiert werden, so dass alle Klassen, die die Klasse BestellungController implementieren, ebenfalls mit Controller enden. Oder dass alle Subklassen von *[JooqAggregateRepository]*{en} nur finale Felder besitzen.

(pause: 0.5)

Zu guter Letzt kann man speziell annotierte Klassen überprüfen, wie etwa, dass mit Controller annotierte Klassen nur im Package Controller liegen.

(pause: 1)
---
(font-size: 30)
````md
# LANG API: Verkettung & Verschachtelung von Prädikaten & Bedingungen

```java
@ArchTest
static final ArchRule myRule = classes()
        .that().resideInAPackage("..repository..")
                        // Verschachtelung
        .and().implement(simpleNameEndingWith("Repository"))
        .or().resideInAPackage("..service..")
        .should().beAnnotatedWith(Controller.class)
        .orShould().beAnnotatedWith(Repository.class)
        .andShould().haveOnlyFinalFields();
```
````

Zusätzlich zum Definieren von Regeln können noch verschiedene Prädikate oder auch Bedingungen aneinandergekettet und oder verschachtelt werden. Man sollte dennoch versuchen, die Testfunktionen so knapp und präzise wie möglich auszudrücken. Oft ist eine Aneinanderkettung von Prädikaten und Bedingungen ein Zeichen für schlechtes Testdesign und kann vermieden werden. Die Funktionalität der zu sehenden Funktion wäre besser in 4 verschiedene Testfunktionen aufgeteilt, damit auch der automatisch erstellte Regeltext, der als Fehlermeldung ausgegeben wird, zielführender ist.

(pause: 1)
---
(font-size: 30)
````md
# LANG API: Veränderung des Regeltextes

```java
@ArchTest
static final ArchRule myRule = classes()
        .that().resideInAPackage("..controller..")
        .should().beAnnotatedWith(Controller.class)
        .because("Common Naming Convention");

@ArchTest
static final ArchRule myRule = noClasses()
        .that().resideInAPackage("..controller..")
        .should().beAnnotatedWith(Controller.class)
        .as("Don't use spring stereotype annotation");        

```
````

Wenn dennoch komplexere Regeln definiert werden müssen, ist es sinnvoll den Grund für diese Regel zu dokumentieren. Dies kann man mit der Funktion *[because]{en}* erreicht werden, die sonst keine weitere Funktionalität anstößt. Darüber hinaus ist es außerdem möglich die automatische Regelbeschreibung vollständig zu überschreiben mit der Funktion *[as]{en}*.
 
(pause: 1)
---
![contain](slides/archunit-introduction/Folie05.png)

Neben Klassen können außerdem explizit Members, Felder, Methoden, CodeUnits oder Konstruktoren überprüft werden. Auch hier existieren jeweils die Negationen des Subjekts. Je nach Subjekt können verschiedene Prädikate aufgerufen werden. Dabei stellt die Lang-API eine vielzahl vordefinierter Prädikate und Bedingungen zur Verfügung. Zusätzlich teilen sich einige Subjekte verschiedene Prädikats- und Bedingungsmethoden wie z.B. *[haveNameContaining]{en}*. Ähnliche Objekte wie Member oder Field haben dabei auch Überschneidungen in Ihren Prädikaten und Bedingungen.

(pause: 1)
---
(font-size: 30)
````md
# LANG API: Definieren eigener Prädikate und Bedingungen

```java
DescribedPredicate<TschawaClass> resideInAPackageService = // definiere Prädikat
ArchCondition<TschawaClass> accessClassesThatResideInAPackageController = // definiere Bedingung

@ArchTest
static final ArchRule myRule = noClasses().that(resideInAPackageService)
    .should(accessClassesThatResideInAPackageController);
```
````

Der vollständigkeitshalber sei hier erwähnt, dass wenn die vordefinierte API es nicht erlaubt, ein bestimmtes Konzept auszudrücken, es möglich ist, sie auf beliebige Weise zu erweitern. Dazu kann man mittels *[DescribePredicate]{en}* und [ArcCondition]{en}* eigene Prädikate oder Bedingungen implementieren, die in einer Regel genutzt werden können.


(pause: 1)
---
```md
# LIBRARY API
1. Slices
2. Architectures
3. PlantUmL
4. General Coding Rules
```

Die Library-API bietet eine wachsende Sammlung vordefinierter Regeln, die eine prägnantere API für komplexere Konzepte bietet. Dazu gehören sogenannte Slices und vorgefertigten Architekturregeln. Außerdem ist es möglich Plant U-M-L Diagramme als Basis für automatisch generierte Regeln zu nutzen. Zusätzlich stellt Arc-unit noch generelle Coding Regel zur Verfügung, die beispielsweise global auf ein Projekt angewendet werden können.


(pause: 1)
---
(font-size: 30)
````md
# LIBRARY API: Slices
```java

// Auf Package Ebene
@ArchTest
static final ArchRule = slices().matching("..seed.(*)..").should().notDependOnEachOther()

// Auf SubPackage Ebene
@ArchTest
static final ArchRule = slices().matching("..seed.(**)").should().beFreeOfCycles()

// Zwischen 2 Paketen
@ArchTest
static final ArchRule = slices().matching("..seed.(**).service..").should().notDependOnEachOther()
```
````

Slices sind im Grunde Regeln, die den Code nach Paketen aufteilen und Bedingungen zu diesen sogenannten Slices enthalten. Die API basiert auf der Idee, Klassen nach einem oder mehreren Paketinfixen in Slices zu sortieren. Es werden 2 slice regeln zur Verfügung gestellt. Man kann Prüfen, ob Slices voneinander abhängig sind oder ob sie zyklische Abhängigkeiten haben. Die Matching Funktion definiert dabei die Slices. 

Man kann beispielsweise Slices auf einer Package Ebene einteilen. Die definierten Slices werden in diesem Beispiel dann auf Abhängigkeiten untereinander überprüft.

Darüber hinaus ist es möglich, alle SubPackages als Slices zu definieren und diese dann beispielsweise auf zyklische Abhängikeiten untereinander zu checken. 

Zu guter Letzt kann man eine beliebige Anzahl an Packages zwischen 2 definierten Paketen als Slices zu definieren.

(pause: 1)
---
(font-size: 30)
````md
# LIBRARY API: Layered Architecture
```java
@ArchRule
static final Architectures.LayeredArchitecture assertCleanArchitecture =  layeredArchitecture()
        .layer("Controller").definedBy("..controller..")
        .layer("Service").definedBy("..service..")
        .layer("Repository").definedBy("..reporitory..")

        .whereLayer("Controller").mayNotBeAccessedByAnyLayer()
        .whereLayer("Service").mayOnlyBeAccessedByLayers("Controller")
        .whereLayer("Repository").mayOnlyBeAccessedByLayers("Service")
        .ignoreDependency(resideInAPackage("..configuration.."), DescribedPredicate.alwaysTrue())
```
````

Eine recht typische Architekturregel ist die Trennung von Anwendungsschichten und deren klar geregelten Abhängigkeitsbeziehungen. Vermischt mit Code-Vererbung kann eine entsprechende Regelverletzung in einem Review auch mal übersehen werden. Arc-unit bietet hier eine explizite Unterstützung, die es ermöglicht, Schichten durch Package-Regeln zu definieren und ihre Zugriffe zu prüfen. In der zu sehenden ArchitekturRegel ist dabei definiert, dass auf die Controller Schicht keine anderen Schichten zugreifen darf. Auf die Service Schicht darf nur die Controllerschicht zugreifen und auf die Repository Schicht nur der Service. Klassen in dem PAckage Configuration werden dabei ignoiert, da sie Abhängikeiten in alle Klassen vorweist.

(pause: 1)
---
(font-size: 30)
````md
# LIBRARY API: Onion Architecture
```java
@ArchTest
static final Architectures.OnionArchitecture onionArchitecture = onionArchitecture()
        .domainModels("..core.domain..")
        .domainServices("..core.domain.service..")
        .applicationServices("..core.config..")
        .adapter("Rest","..inbound.rest..")    
        .adapter("JooQ", "..outbound.jooq..");
```
````

Neben der klassischen Schichtenarchitektur gibt es auch die sogenannte OnionArchitektur auch bekannt als Ports and Adapters oder Hexagonal-architektur.
Grundidee ist die eigentliche Fachlichkeit einer Anwendung in ihren Kern zu stellen und Infrastruktur sowie alle externen Verbindungen zu kapseln, um sie voneinander zu entkoppeln. Arc-unit bietet hierfür eine explizite Unterstützung angelehnt an den Begriff der Onion-Architecture, die wie im folgenden Code-Beispiel genutzt werden kann. Dabei wird unter anderem sichergestellt, dass die Adapter keine Abhängigkeiten in den inneren Kern der Anwendung haben.

(pause: 1)
---
(font-size: 30)
````md
# LIBRARY API: PlantUML Rules
```java
@ArchTest
static final void plantUmlRule(final JavaClasses importedClasses) {
    final URL plantUmlDiagram = PlantUmlRulesTest.class.getResource("component_diagram.puml");

    assert plantUmlDiagram != null;

    classes()
        .should(adhereToPlantUmlDiagram(plantUmlDiagram, consideringOnlyDependenciesInDiagram())
        .ignoreDependenciesWithOrigin(
            GET_PACKAGE_NAME.is(PackageMatchers.of("..configuration.."))
                .as("configuration is used everywhere"))
        )
        .check(importedClasses);
}
```
````

Falls die Systemarchitektur nicht in die zuvor gezeigten Architekturstrukturen passen oder man sich die Definition von programmatischen Regeln sparen will, besteht die Möglichkeit, ein Plant U-M-L Diagramm als Basis für die Regeldefinition zu nutzen. Dabei werden die zu prüfenden Strukturen einfach in einer Plant U-M-L Datei definiert und importiert. In dem zu sehende Code-Snippet wird dabei zusätzlich das Package Configuration ignoriert, da es in der Definiereten Plant U-m-l-Datei nicht vorkommt.

(pause: 1)
---
(font-size: 30)
````md
# PlantUML Rules
```java
// General Coding Rules
@ArchTest
final static ArchRule noClassesShouldUseStandardLogging = noClasses()
    .should(GeneralCodingRules.USE_JAVA_UTIL_LOGGING);

@ArchTest
final static  ArchRule noClassesShouldUseJodaTime = noClasses()
    .should(GeneralCodingRules.USE_JODATIME);

// ProxyRules
@ArchTest
final static ArchRule no_bypass_of_proxy_logic =
        no_classes_should_directly_call_other_methods_declared
        _in_the_same_class_that_are_annotated_with(Async.class);

...      
```
````

Die Klasse GeneralCodingRules enthält eine Reihe von sehr allgemeinen Regeln und Bedingungen für die Codierung. Beispielsweise um zu überprüfen, dass Klassen kein tschawa-util-logging, sondern andere Bibliotheken wie self-four-jay oder dass Klassen nicht tschoda-Time verwenden, sondern stattdessen java.time. Außerdem können Proxy-Regeln definiert werden, Um zu überprüfen, dass Methoden, die einem Prädikat entsprechen, nicht direkt aus derselben Klasse aufgerufen werden. 

(pause: 1)
---
```md
# WEITERE FUNKTIONALITÄTEN
- Software Architecture Metrics
- Freezing Arch Rules
- ...
```

Neben der Funktionalität zur automatischen Überprüfung von Code stellt Arc-unit noch ein paar weitere Funktipnalitäten zur Verfügung. Dazu gehören beispielsweise Software Architecture Metrics, die ähnlich wie Code-Qualitätsmetriken, wie z.B. zyklomatische Komplexität oder Methodenlänge, danach streben, die Struktur und das Design von Software zu messen. Arc-unit kann dabei einige bekannte Softwarearchitekturmetriken berechnen. 

Wenn in gewachsenen Projekten Regeln eingeführt werden, gibt es oft Hunderte oder gar Tausende von Verstößen, viel zu viele, um sie sofort zu beheben. Die einzige Möglichkeit, solche umfangreichen Verstöße zu beheben, ist ein iterativer Ansatz, der eine weitere Verschlechterung der Codebasis verhindert. Freezing-Arc-Rule kann in diesen Szenarien helfen, indem es alle bestehenden Verstöße in einem ViolationStore aufzeichnet. 

(pause: 1)
---
![contain](slides/archunit-introduction/Folie06.png)

Zusammenfassend lässt sich sagen, dass mit Arc-unit automatische Architekturtests Strukturen eines Systems überprüft werden können, was zu einer effizienteren Entwicklung beitragen kann. Dabei können Objekte in Form von Klassen, Feldern, Methoden, Member, Konstruktoren oder ganzen Code-Units auf Ihre interne Struktur sowie auf Beziehungen untereinander mit Hilfe einfacher Syntax überprüft werden. Für komplexere Vorgänge gibt es vorgefertige Architekturen oder die Möglichkeit eigene Regeln zu definieren. Zusätzlich biete Arc-unit einige weitere Features an, die die Nutzung von Arc-unit vereinfachen oder die Möglichkeit mit Arc-unit durchzustarten bieten.

weitere Infos findet Ihr in den Folgevideos, sowie der Arc-unit-Dokumentation.
Vielen Dank für eure Zeit und bis bald!


